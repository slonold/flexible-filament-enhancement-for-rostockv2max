# Flexible Filament Enhancements for RostockV2Max

Documentation of modifications made to a SeeMeCNC Rostock V2 Max fused filament fabrication delta 3D printer in order to improve flexible filament printing performance.   